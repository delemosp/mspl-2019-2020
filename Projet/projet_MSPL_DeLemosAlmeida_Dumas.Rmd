---
title: "Tailles et PIB des humains par pays"
author: "De Lemos Almeida Pierre, Dumas Clément"
date: 2020
output: pdf_document
---
# Table des matières
- Introduction
  - Contexte et Description du Dataset
  - Description de la question
- Méthodologie
  - Procédures de nettoyage des données
  - Flux de travail scientifique
  - Choix de représentation des données
- Analyse en programmation littéraire
- Conclusion
- Références

\newpage
--
Introduction
--
# Contexte et Description du Dataset

Dans ce projet nous utilisons deux Dataset.

## Premier Dataset

La premiere est disponible sur le site de La Banque Mondiale et est intitulé "Indicateurs du développement dans le monde".

Les données correspondent donc au PIB en dollars américains courants (c'est à dire la valeur du dollars sur l'année en question) de chaque pays de chaque année depuis l'année 1960 jusqu'à 2019.

La base de donnée est découpé en plusieurs colonnes :

  - Pays
  - Le code du pays en question
  - Nom de l'indicateur la monnaie utilisé (qui est toujours $ US courants)
  - Code de l'incateur de monnaie correspondant
  - Les colonnes de chaque années de 1960 à 2019 avec les valeurs de PIB
  
## Second Dataset
  
Le second est disponible sur le site Our World in Data et est intitulé "Average height by year of birth".
  
Les données correspondent donc à la moyenne de la taille de chaque pays et de chaque année depuis 1896 jusqu'à 1996 et differencié par sexe.

La base de donnée est découpé en plusieurs colonnes :

  - Pays
  - Le code du pays en question
  - Les années
  - La taille des hommes en centimètre
  - La taille des femmes en centimètre
  
# Description de la question

Le projet a pour objectif de répondre à la problèmatique **Le niveau de richesse (pib) d'un pays influe-t il sur la taille de la population ?**

Notre but est donc de voir si la croissance de la taille des hommes et des femmes d'un pays est impacté par son niveau de richesse.

\newpage

# Méthodologie

## Procédures de nettoyage des données

Afin de pouvoir exploiter notre données nous avons besoin d'un Dataset or nous en avons deux, notre but premier est donc de les regrouper en un unique. Mais nos deux differents Dataset ne sont pas organisés de la même manière
- Première étape, nous devons donc faire en sortent de les réorganisés de la même manière et nous pouvons en profiter pour enlever les colonnes qui ne nous servirons pas comme les codes des pays, le nom et code de l'indicateur de monnaie.
- Seconde étape, nous devons fusionné les deux Dataset finaux de l'étape une.

## Flux de travail scientifique

## Choix de représentation des données

# Analyse en programmation littéraire
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r}
#Recupération du fichiers ZIP sur la taille si ils ne sont pas présent dans le dossier
file = "API_NY.GDP.PCAP.CD_DS2_fr_csv_v2_838225.zip"
if(!file.exists(file)){
  download.file("http://api.worldbank.org/v2/fr/indicator/NY.GDP.PCAP.CD?downloadformat=csv",
	destfile=file)
}
#On décompresse le fichier ZIP
unzip(file)

#Recupération du fichiers CSV sur le PIB si ils ne sont pas présent dans le dossier
fileCsv = file = "API_NY.GDP.PCAP.CD_DS2_fr_csv_v2_838225.csv"
if(!file.exists(fileCsv)){
  download.file("blob:https://ourworldindata.org/80daad88-1494-44ed-add3-555211d29bd3",
	destfile=fileCsv)
}

#Recupération des librairie
library(tidyr)
library(tidyverse)
library(ggplot2)
library(gganimate)
library(tinytex)
library(gifski)

#On place les CSV dans des DataSet
dPib <- read_delim("API_NY.GDP.PCAP.CD_DS2_fr_csv_v2_838225.csv",delim=",", skip =4);
dTaille <- read_delim("average-height-by-year-of-birth.csv",delim=",");

# On recupere que les données qui nous interesse 
dPibFilter = subset(dPib, select=-(2:4))

# On supprime certaines colonnes
dTailleFilter = subset(dTaille, select=-2)
# On renomme les colonnes de façon à pouvoir les utiliser
names(dTailleFilter) <- c("pays", "annee", "homme", "femme")

##On formate les données pour qu'elle soit pareil dans les deux dataframe
dPibFormater <- dPibFilter %>% 
                        select('Country Name', 
                               starts_with("19") | starts_with("20") ) %>% 
                        gather(-'Country Name',
                               key="année",
                               value="PIB")
names(dPibFormater) <- c("pays", "annee", "pib")
dPibFormater <- mutate(dPibFormater, annee = as.numeric(annee))

#Fusion des deux Dataset
dFusion <- merge(dTailleFilter, dPibFormater, by="pays")
#On supprime des lignes afin d'avoir une bonne association entre les deux Dataset
dFusion <- filter(dFusion, annee.y == annee.x) 
#On retire une colonne annee qui est en double
dFusion <- dFusion[!(names(dFusion) %in% "annee.y")]
#On renomme les colonnes
names(dFusion) <- c("pays", "annee", "cmHomme", "cmFemme", "pib")
dFusion <- mutate(dFusion, rapportHomme = pib/cmHomme);
dFusion <- mutate(dFusion, rapportFemme = pib/cmFemme);
head(dFusion)
```

```{r}
plotPIB <- ggplot(dFusion,aes(x=annee,y=pib,color=pays))+
  geom_line()+
  labs(title="Le PIB de chaque Pays par année", x= "Année", y="PIB",caption ="Source : La Banque Mondiale") +
  theme(legend.position = "None")
plotHomme <- ggplot(dFusion,aes(x=annee,y=cmHomme,color=pays))+
  geom_line()+
  labs(title="La taille des hommes de chaque Pays par année", x= "Année", y="Taille des hommes (cm)",caption ="Source : Our World in Dat") +
  theme(legend.position = "None")

plotFemme <- ggplot(dFusion,aes(x=annee,y=cmFemme,color=pays))+
  geom_line()+
  labs(title="La taille des femmes de chaque Pays par année", x= "Année", y="Taille des femmes (cm)",caption ="Source : Our World in Dat") +
  theme(legend.position = "None")

```

```{r}
#Affichage
ggplot(dFusion, aes(x=rapportHomme, y=pays, color=pays))+
  geom_point() +
  labs(title="Rapprt entre la taille des hommes et le PIB de chaque pays", subtitle="En {closest_state}", x="Rapport pib / taille des hommes en cm", y="Pays", caption="Source : Our World in Data, La Banque Mondiale")+ 
  theme(legend.position = "None") +
  transition_states(states = annee, transition_length = 1, state_length = 1)
```

```{r}

```
